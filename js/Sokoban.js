var Sokoban = (function() {
    'use strict';
    const API_URL = 'https://sokoban.doonoo.fr/';

    return {
        /**
         * @throws an Error if onSuccess or an Error is undefined
         * @param {function} onSuccess
         * @param {function} onError
         */
        fetchAllLevelCollections: function({onSuccess, onError}) {
            if (typeof(onSuccess) == 'undefined' || typeof (onError) == 'undefined')
                throw new Error('Sokoban.fetchAllLevelCollections: Undefined function');

            $.ajax({
                url: API_URL,
                method: 'GET',
                dataType: 'json',
                success: onSuccess,
                error: onError,
            });
        },
        fetchLevelCollectionById: function({id, onSuccess, onError}) {
            if (typeof(id) == 'undefined' || typeof(onSuccess) == 'undefined' || typeof (onError) == 'undefined')
                throw new Error('Sokoban.fetchlevelCollectionById: Undefined parameter');

            $.ajax({
                url: API_URL + 'levels/' + id,
                method: 'GET',
                dataType: 'json',
                success: onSuccess,
                error: onError,
            });
        },
        UI: {
            Menu: class {
                /**
                 *
                 * @param {string} title
                 * @param {Entry[]} entries
                 */
                constructor({title, entries}) {
                    this.title = title;
                    this.entries = entries || [];
                }

                show() {
                    let $list = $('<ul />')
                        .addClass('sokoban-menu-list');

                    this.entries.forEach((e) => {
                        $list.append(e.show());
                    });

                    return $('<div />')
                        .addClass('sokoban-menu')
                        .append(
                            $('<header />')
                                .text(this.title)
                        )
                        .append($list);
                }
            },
            Entry: class {
                /**
                 * Creates an Entry that can be rendered through a jQuery element
                 * @param title
                 * @param description
                 * @param copyright
                 * @param levelCount
                 * @param {function} onClick
                 */
                constructor({title, description, copyright, levelCount, onClick}) {
                    this.title = title;
                    this.description = description;
                    this.attribution = '© ' + copyright;
                    this.levelCount = levelCount;
                    this.onClick = onClick;
                }

                /**
                 * returns jQuery element
                 * @returns {jQuery}
                 */
                show() {
                    return $('<div />')
                        .addClass('sokoban-entry')
                        .append(
                            $('<header />').text(this.title)
                        )
                        .append(
                            $('<p />').text(this.description)
                        )
                        .append(
                            $('<p />').text(this.attribution)
                        )
                        .append(
                            $('<p />').text(this.levelCount)
                        )
                        .append(
                            $('<button />')
                                .text('Play')
                                .click(() => this.onClick(this))
                        )
                }
            },
            LevelEntry: class {

                constructor(cells) {
                    this.cells = cells;
                }

                show() {
                    let $root = $('<div />');

                    for (let i = 0; i < this.cells.length; ++i) {
                        $root.append(
                            $('<div />')
                                .text(this.cells[i])
                        );
                    }

                    return $root
                        .addClass('sokoban-entry')
                        .addClass('sokoban-level');
                }
            }
        },
    };
})();
