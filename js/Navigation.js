var Navigation = (function() {
    'use strict';

    return {
        Navigation: class {
            /**
             *
             * @param {Navigation.Item} root
             * @param {function?} willNavigate
             * @param {function?} didNavigate
             */
            constructor({root, willNavigate, didNavigate}) {
                this.root = root;
                this.willNavigate = willNavigate;
                this.didNavigate = didNavigate;

                this.queue = [];

                this.navigate();
            }

            navigate() {
                if (typeof(this.willNavigate) != "undefined")
                    this.willNavigate();

                this.current().navigate();

                if (typeof(this.didNavigate) != "undefined")
                    this.didNavigate();
            }

            current() {
                if (this.queue.length === 0)
                    return this.root;

                return this.queue[this.queue.length - 1];
            }

            push(newItem) {
                this.queue.push(newItem);
                this.navigate();
            }

            previous() {
                if (this.queue.length === 0)
                    throw new Error("Can't use previous when queue is empty.");

                this.queue.pop();
                this.navigate();
            }
        },

        Item: class {
            constructor({title, onNavigate}) {
                this.title = title;
                this.onNavigate = onNavigate;
            }

            navigate() {
                this.onNavigate();
            }
        }
    };
})();
