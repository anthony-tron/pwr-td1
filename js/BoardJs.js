var BoardJs = (function () {

    return {
        Cell: class Cell {
            display() {
                return $('<td />');
            }
        },

        Board: class Board {

            /***
             * @param rows : number
             * @param cols : number
             */
            constructor(rows, cols) {
                this.rows = rows;
                this.cols = cols;
                this.currentPlayer = 'X';
            }

            /**
             * Returns next player's representation
             * @returns {string}
             */
            nextPlayer() {
                return this.currentPlayer === 'X' ? 'O' : 'X';
            }

            /**
             * Returns jQuery's representation of the table
             * @returns {*|jQuery}
             */
            display() {
                let $tbody = $('<tbody />');

                for (let i = 0; i < this.rows; ++i) {
                    let $tr = $('<tr />');

                    for (let j = 0; j < this.cols; ++j) {
                        let cell = new BoardJs.Cell();
                        let self = this;

                        $tr.append(
                            cell.display()
                                .click(function() {
                                    $(this).text(self.currentPlayer);
                                    self.currentPlayer = self.nextPlayer();
                                })
                            );
                    }

                    $tbody.append($tr);
                }

                return $('<table />')
                    .addClass('board')
                    .addClass('noselect')
                    .append($tbody);
            }
        }
    }

})();