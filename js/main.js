(function () {
    'use strict';

    $(() => {
        let $levelGroups = $("#level-groups");
        let $levelLists = $("#level-lists");
        let $previousButton = $("#previous-btn");

        let navigation = new Navigation.Navigation({
            root: new Navigation.Item({
                title: 'Menu principal',
                onNavigate: function () {
                    $levelGroups.show();
                    $levelLists.hide();
                },
            }),
            didNavigate: function () {
                $previousButton.prop('disabled', this.queue.length === 0);
            },
        });

        $previousButton.click(() => navigation.previous());
        // TODO handle exception

        Sokoban.fetchAllLevelCollections({
            onSuccess: (data) => {

                const entries = Object.entries(data)
                    .map(([key, content]) => new Sokoban.UI.Entry({
                        title: content.title,
                        description: content.description,
                        copyright: content.copyright,
                        levelCount: content.total,
                        onClick: () => {
                            Sokoban.fetchLevelCollectionById({
                                id: key,
                                onSuccess: (data) => {
                                    if (data.hasOwnProperty('levels')) {
                                        const levelEntries = Object.values(data.levels).map((level) => {
                                            if (level.hasOwnProperty('cells')) {
                                                return new Sokoban.UI.LevelEntry(level.cells);
                                            }
                                        });

                                        let levelsMenu = new Sokoban.UI.Menu({
                                            title: content.title,
                                            entries: levelEntries,
                                        });

                                        $levelLists.html(
                                            levelsMenu.show()
                                        );

                                        navigation.push(new Navigation.Item({
                                            title: content.title,
                                            onNavigate: function() {
                                                $levelGroups.hide();
                                                $levelLists.show();
                                            },
                                        }))
                                    }
                                },
                                onError: (error) => console.error(error)
                            });
                        },
                    }));

                const menu = new Sokoban.UI.Menu({
                    title:'Menu',
                    entries: entries,
                });
                $levelGroups.append(menu.show());
            },
            onError: (error) => console.error(error),
        });
    });
})();
